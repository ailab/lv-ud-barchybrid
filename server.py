#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, pickle
from flask import Flask, jsonify, request
from flask_cors import CORS

from barchybrid.arc_hybrid import ArcHybridLSTM
from barchybrid import upostag

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger('api')

MODEL = os.getenv('MODEL', 'models/barchybrid-ud-v7/barchybrid.model25')
PARAMS = os.getenv('PARAMS', os.path.join(os.path.dirname(MODEL), 'params.pickle'))
PORT = os.getenv('PORT', '9011')
HOST = os.getenv('HOST', '0.0.0.0')


def get_parser(model, params):
    with open(params, 'rb') as paramsfp:
        words, w2i, pos, rels, stored_opt = pickle.load(paramsfp)
    parser = ArcHybridLSTM(words, pos, rels, w2i, stored_opt)
    parser.Load(model)
    return parser


def process_doc(parser, doc):
    for sentence in doc['sentences']:
        tokens = [{
            'form': t.get('text') or t.get('form'),
            'pos': t.get('tag'),
            'cpos': t.get('tag')
        } for t in sentence['tokens']]
        res_tokens = parser.predict_sentence(tokens)

        for t, rt in zip(sentence['tokens'], res_tokens):
            t['index'] = rt['index']
            t['parent'] = rt['parent']
            t['deprel'] = rt['deprel']
            t['upos'] = upostag.get_synt_upostag(t.get('lemma'), t.get('tag'), rt['deprel'])
            t['ufeats'] = upostag.feats_str(upostag.get_ufeats(t.get('lemma'), t.get('tag')))
    return doc


app = Flask(__name__)
CORS(app)

logger.info('load model')

app.model = get_parser(MODEL, PARAMS)
logger.info('model loaded')


@app.route('/parser', methods=['POST'])
def parse():
    try:
        req = request.get_json(force=True, silent=True)
        return jsonify(dict(data=process_doc(app.model, req['data'])))
    except Exception as e:
        logger.exception('Unable to process')
        return jsonify(dict(error=repr(e)))


if __name__ == '__main__':
    print('RUN API %r %r' % (HOST, PORT))
    app.run(host=HOST, port=int(PORT), debug=False, threaded=False)
