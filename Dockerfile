FROM artursz/dynet:v1

RUN apt-get update && apt-get install -y --no-install-recommends \
    curl unzip \
    && rm -fr /var/lib/apt/lists/*

WORKDIR /app

COPY requirements.txt /app/

RUN pip install -r requirements.txt

COPY barchybrid /app/barchybrid/
COPY server.py /app/

RUN curl "http://nlp.ailab.lv:9090/barch/lvtb2.3_spos-barch-v3.zip" --output model.zip \
    && mkdir -p /app/models/model \
    && unzip model.zip -d /app/models/model \
    && rm model.zip

ENV MODEL=/app/models/model/barchybrid.model
ENV PORT=80
ENV LANG C.UTF-8
ENV PYTHONUNBUFFERED=1

CMD if [ ${PY_ENV} = "dev" ]; then \
    python main.py; \
else \
    gunicorn server:app -w ${GUNICORN_WORKERS:=1} -t ${GUNICORN_TIMEOUT:=60} --graceful-timeout ${GUNICORN_TIMEOUT:=60} -b ${HOST:=0.0.0.0}:${PORT:=5000} ${GUNICORN_RELOAD:=""} --log-level ${GUNICORN_LOG_LEVEL:=info}; \
fi
