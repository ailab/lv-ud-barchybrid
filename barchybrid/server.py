from barchybrid.arc_hybrid import ArcHybridLSTM
import pickle, os, time, sys, json

MODEL = '../models/barchybrid-ud-v4/barchybrid.model30'
PARAMS = '../models/barchybrid-ud-v4/params.pickle'

with open(PARAMS, 'rb') as paramsfp:
    words, w2i, pos, rels, stored_opt = pickle.load(paramsfp)
parser = ArcHybridLSTM(words, pos, rels, w2i, stored_opt)
parser.Load(MODEL)

ts = time.time()
sentences = [
    {
      "tokens": [
        {
          "form": "Pēc",
          "tag": "sppd"
        },
        {
          "form": "zāļu",
          "tag": "ncfpg5"
        },
        {
          "form": "ražotājas",
          "tag": "arfpnnp"
        },
        {
          "form": "AS",
          "tag": "y"
        },
        {
          "form": "\"",
          "tag": "zq"
        },
        {
          "form": "Olainfarm",
          "tag": "xx"
        },
        {
          "form": "\"",
          "tag": "zq"
        },
        {
          "form": "lielākā",
          "tag": "afmsgyc"
        },
        {
          "form": "īpašnieka",
          "tag": "ncmsg1"
        },
        {
          "form": "un",
          "tag": "ccs"
        },
        {
          "form": "valdes",
          "tag": "ncfsg5"
        },
        {
          "form": "priekšsēdētāja",
          "tag": "n_fsn_"
        },
        {
          "form": "Valērija",
          "tag": "xx"
        },
        {
          "form": "Maligina",
          "tag": "xf"
        },
        {
          "form": "aiziešanas",
          "tag": "ncfsg4"
        },
        {
          "form": "mūžībā",
          "tag": "ncfsl4"
        },
        {
          "form": "valdes",
          "tag": "ncfsg5"
        },
        {
          "form": "locekļa",
          "tag": "ncmsg2"
        },
        {
          "form": "amatu",
          "tag": "ncmsa1"
        },
        {
          "form": "nolēmis",
          "tag": "vmnpdmsnasn"
        },
        {
          "form": "saglabāt",
          "tag": "v_nn0t2000n"
        },
        {
          "form": "Salvis",
          "tag": "xx"
        },
        {
          "form": "Lapiņš",
          "tag": "n_msn1"
        },
        {
          "form": ".",
          "tag": "zs"
        }
      ]
    },
    {
      "tokens": [
        {
          "form": "Novembra",
          "tag": "ncmsg2"
        },
        {
          "form": "beigās",
          "tag": "ncfdl4"
        },
        {
          "form": "uzņēmums",
          "tag": "ncmsn1"
        },
        {
          "form": "paziņoja",
          "tag": "vmnist230an"
        },
        {
          "form": ",",
          "tag": "zc"
        },
        {
          "form": "ka",
          "tag": "css"
        },
        {
          "form": "Lapiņš",
          "tag": "n_msn1"
        },
        {
          "form": "nākamā",
          "tag": "a_msgy"
        },
        {
          "form": "gada",
          "tag": "ncmsg1"
        },
        {
          "form": "janvārī",
          "tag": "ncmsl2"
        },
        {
          "form": "atstās",
          "tag": "vmnift130an"
        },
        {
          "form": "farmācijas",
          "tag": "n_fsg4"
        },
        {
          "form": "uzņēmuma",
          "tag": "ncmsg1"
        },
        {
          "form": "valdes",
          "tag": "ncfsg5"
        },
        {
          "form": "locekļa",
          "tag": "ncmsg2"
        },
        {
          "form": "amatu",
          "tag": "ncmsa1"
        },
        {
          "form": ".",
          "tag": "zs"
        }
      ]
    }
  ]
print(json.dumps(parser.predict_sentences(sentences), indent=2))
te = time.time()