from optparse import OptionParser
from barchybrid.arc_hybrid import ArcHybridLSTM
from barchybrid import utils
import pickle, os, time, sys, re, datetime


def get_conll_txt_res(fpath, formatted=True):
    r = dict()
    try:
        with open(fpath) as f:
            lines = f.readlines()
        for l in lines:
            m = re.match('^(UAS|LAS|WeightedLAS)\s*\|\s*(?:\d+\.\d+)\s*\|\s*(?:\d+\.\d+)\s*\|\s*(\d+\.\d+)', l)
            if m:
                metric = m.group(1)
                val = float(m.group(2))
                r[metric] = val
    except Exception as e:
        print('Unable to read {}: {}'.format(fpath, e))

    if formatted:
        return '\t'.join([str(e) for e in [r.get('LAS', -1), r.get('UAS', -1), r.get('WeightedLAS', -1)]])

    return r

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("--train", dest="conll_train", help="Annotated CONLL train file", metavar="FILE", default="../data/PTB_SD_3_3_0/train.conll")
    parser.add_option("--dev", dest="conll_dev", help="Annotated CONLL dev file", metavar="FILE", default="../data/PTB_SD_3_3_0/dev.conll")
    parser.add_option("--test", dest="conll_test", help="Annotated CONLL test file", metavar="FILE", default="../data/PTB_SD_3_3_0/test.conll")
    parser.add_option("--params", dest="params", help="Parameters file", metavar="FILE", default="params.pickle")
    parser.add_option("--extrn", dest="external_embedding", help="External embeddings", metavar="FILE")
    parser.add_option("--model", dest="model", help="Load/Save model file", metavar="FILE", default="barchybrid.model")
    parser.add_option("--wembedding", type="int", dest="wembedding_dims", default=100)
    parser.add_option("--pembedding", type="int", dest="pembedding_dims", default=25)
    parser.add_option("--rembedding", type="int", dest="rembedding_dims", default=25)
    parser.add_option("--epochs", type="int", dest="epochs", default=30)
    parser.add_option("--hidden", type="int", dest="hidden_units", default=100)
    parser.add_option("--hidden2", type="int", dest="hidden2_units", default=0)
    parser.add_option("--k", type="int", dest="window", default=3)
    parser.add_option("--lr", type="float", dest="learning_rate", default=0.1)
    parser.add_option("--outdir", type="string", dest="output", default="results")
    parser.add_option("--activation", type="string", dest="activation", default="tanh")
    parser.add_option("--lstmlayers", type="int", dest="lstm_layers", default=2)
    parser.add_option("--lstmdims", type="int", dest="lstm_dims", default=200)
    parser.add_option("--dynet-seed", type="int", dest="seed", default=7)
    parser.add_option("--disableoracle", action="store_false", dest="oracle", default=True)
    parser.add_option("--disableblstm", action="store_false", dest="blstmFlag", default=True)
    parser.add_option("--bibi-lstm", action="store_true", dest="bibiFlag", default=False)
    parser.add_option("--usehead", action="store_true", dest="headFlag", default=False)
    parser.add_option("--userlmost", action="store_true", dest="rlFlag", default=False)
    parser.add_option("--userl", action="store_true", dest="rlMostFlag", default=False)
    parser.add_option("--predict", action="store_true", dest="predictFlag", default=False)
    parser.add_option("--dynet-mem", type="int", dest="cnn_mem", default=512)
    parser.add_option("--dynet-devices", type="str", default='')

    (options, args) = parser.parse_args()


    class Logger:
        def __init__(self, filename):
            self.stdout = sys.stdout
            self.logfile = open(filename, 'a')

            import atexit
            atexit.register(self.close)

        def write(self, text):
            self.stdout.write(text)
            self.logfile.write(text)
            self.flush()

        def close(self):
            self.stdout.close()
            self.logfile.close()

        def flush(self):
            self.stdout.flush()
            self.logfile.flush()

    if options.output:
        os.makedirs(options.output, exist_ok=True)
        w = Logger(os.path.join(options.output, 'log.txt'))
        sys.stdout = w
        sys.stderr = w

    print('ARGS', sys.argv)
    print('OPTIONS', options)
    print('Started', datetime.datetime.now())

    print('Using external embedding:', options.external_embedding)

    if not options.predictFlag:
        if not (options.rlFlag or options.rlMostFlag or options.headFlag):
            print('You must use either --userlmost or --userl or --usehead (you can use multiple)')
            sys.exit()

        print('Preparing vocab')
        words, w2i, pos, rels = utils.vocab(options.conll_train)

        os.makedirs(options.output, exist_ok=True)
        with open(os.path.join(options.output, options.params), 'wb') as paramsfp:
            # pickle.dump((words, w2i, pos, rels, options), paramsfp)
            pickle.dump([words, w2i, pos, rels, options], paramsfp)
        print('Finished collecting vocab')

        print('Initializing blstm arc hybrid:')
        parser = ArcHybridLSTM(words, pos, rels, w2i, options)

        res_tsv = open(os.path.join(options.output, 'res.csv'), 'w')
        res_tsv.write('epoch\tLAS\tUAS\tWeightedLAS\tLAS\tUAS\tWeightedLAS\n')

        for epoch in range(options.epochs):
            print('Starting epoch', epoch, datetime.datetime.now())
            parser.Train(options.conll_train, epoch=epoch)
            conllu = (os.path.splitext(options.conll_dev.lower())[1] == '.conllu')
            devpath = os.path.join(options.output, 'dev_epoch_' + str(epoch+1) + ('.conll' if not conllu else '.conllu'))
            utils.write_conll(devpath, parser.Predict(options.conll_dev))

            if not conllu:
                os.system('perl barchybrid/eval/eval.pl -g ' + options.conll_dev  + ' -s ' + devpath  + ' > ' + devpath + '.txt')
            else:
                os.system('python barchybrid/eval/conll17_ud_eval.py -v -w barchybrid/eval/weights.clas ' + options.conll_dev + ' ' + devpath + ' > ' + devpath + '.txt')
            dev_res = get_conll_txt_res(devpath + '.txt')
            print('DEV', dev_res)
            print('Finished predicting dev')

            test_res = '_\t_\t_'
            if options.conll_test:
                ts = time.time()
                tespath = os.path.join(options.output, 'test_epoch_' + str(epoch+1) + ('.conll' if not conllu else '.conllu'))
                utils.write_conll(tespath, parser.Predict(options.conll_test))
                if not (os.path.splitext(options.conll_test.lower())[1] == '.conllu'):
                    os.system('perl barchybrid/eval/eval.pl -g ' + options.conll_test + ' -s ' + tespath + ' > ' + tespath + '.txt')
                else:
                    os.system('python barchybrid/eval/conll17_ud_eval.py -v -w barchybrid/eval/weights.clas ' + options.conll_test + ' ' + tespath + ' > ' + tespath + '.txt')
                test_res = get_conll_txt_res(tespath + '.txt')
                print('TEST', test_res)
                print('Finished predicting dev', time.time()-ts)

            parser.Save(os.path.join(options.output, options.model + str(epoch+1)))
            res_tsv.write('{}\t{}\t{}\n'.format(epoch, dev_res, test_res))
            res_tsv.flush()
        res_tsv.close()
    else:
        with open(options.params, 'rb') as paramsfp:
            words, w2i, pos, rels, stored_opt = pickle.load(paramsfp)

        stored_opt.external_embedding = options.external_embedding

        parser = ArcHybridLSTM(words, pos, rels, w2i, stored_opt)
        parser.Load(options.model)
        conllu = os.path.splitext(options.conll_test.lower())[1] == '.conllu'
        os.makedirs(options.output, exist_ok=True)
        tespath = os.path.join(options.output, 'test_pred.conll' if not conllu else 'test_pred.conllu')
        ts = time.time()
        pred = list(parser.Predict(options.conll_test))
        te = time.time()
        utils.write_conll(tespath, pred)

        if not conllu:
            os.system('perl barchybrid/eval/eval.pl -g ' + options.conll_test + ' -s ' + tespath  + ' > ' + tespath + '.txt')
        else:
            os.system('python barchybrid/eval/conll17_ud_eval.py -v -w barchybrid/eval/weights.clas ' + options.conll_test + ' ' + tespath + ' > ' + tespath + '.txt')
        
        print('Finished predicting test',te-ts)
        print('Finished', datetime.datetime.now())

