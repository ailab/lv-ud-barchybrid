"""
Convert LVTB tags to Universal POS tags
Reimplementation of https://github.com/LUMII-AILab/CorporaTools/blob/master/LVTB2UD/src/lv/ailab/lvtb/universalizer/transformator/morpho/UPosLogic.java
"""

import logging
import re
from enum import Enum
from collections import namedtuple

logger = logging.getLogger(__name__)


class UDv2Relations:
    """https://github.com/LUMII-AILab/CorporaTools/blob/e84fcb252b5664454c7787a4cff84daa2e14175b/LVTB2UD/src/lv/ailab/lvtb/universalizer/conllu/UDv2Relations.java"""
    ACL = "acl"
    ADVCL = "advcl"
    ADVMOD = "advmod"
    AMOD = "amod"
    APPOS = "appos"
    AUX = "aux"
    AUX_PASS = "aux:pass"
    CASE = "case"
    CC = "cc"
    CCOMP = "ccomp"
    CLF = "clf"
    COMPOUND = "compound"
    CONJ = "conj"
    COP = "cop"
    CSUBJ = "csubj"
    CSUBJ_PASS = "csubj:pass"
    DEP = "dep"
    DET = "det"
    DISCOURSE = "discourse"
    DISLOCATED = "dislocated"
    EXPL = "expl"
    FIXED = "fixed"
    FLAT = "flat"
    FLAT_FOREIGN = "flat:foreign"
    FLAT_NAME = "flat:name"
    GOESWITH = "goeswith"
    IOBJ = "iobj"
    LIST = "list"
    MARK = "mark"
    NMOD = "nmod"
    NSUBJ = "nsubj"
    NSUBJ_PASS = "nsubj:pass"
    NUMMOD = "nummod"
    OBJ = "obj"
    OBL = "obl"
    ORPHAN = "orphan"
    PARATAXIS = "parataxis"
    PUNCT = "punct"
    REPARANDUM = "reparandum"
    ROOT = "root"
    VOCATIVE = "vocative"
    XCOMP = "xcomp"


class UDv2PosTag:
    """https://github.com/LUMII-AILab/CorporaTools/blob/ebb55a9223c0e2a7489dbd53ec16d438952d8576/LVTB2UD/src/lv/ailab/lvtb/universalizer/conllu/UDv2PosTag.java"""
    ADJ = 'ADJ'
    ADP = 'ADP'
    ADV = 'ADV'
    AUX = 'AUX'
    CCONJ = 'CCONJ'
    DET = 'DET'
    INTJ = 'INTJ'
    NOUN = 'NOUN'
    NUM = 'NUM'
    PART = 'PART'
    PRON = 'PRON'
    PROPN = 'PROPN'
    PUNCT = 'PUNCT'
    SCONJ = 'SCONJ'
    SYM = 'SYM'
    VERB = 'VERB'
    X = 'X'


feat = namedtuple('KeyValue', ['name', 'value'])
feat.__str__ = lambda self: '%s=%s' % (self.name, self.value)


class UDv2Feat:
    """https://github.com/LUMII-AILab/CorporaTools/blob/b92c740e357426d173905ce29a825dc2deb932c0/LVTB2UD/src/lv/ailab/lvtb/universalizer/conllu/UDv2Feat.java"""
    ABBR_YES = feat("Abbr", "Yes")
    # ANIMACY = "Animacy"),
    ASPECT_IMP = feat("Aspect", "Imp")
    ASPECT_PERF = feat("Aspect", "Perf")

    CASE_NOM = feat("Case", "Nom")
    CASE_ACC = feat("Case", "Acc")
    CASE_DAT = feat("Case", "Dat")
    CASE_GEN = feat("Case", "Gen")
    CASE_LOC = feat("Case", "Loc")
    CASE_VOC = feat("Case", "Voc")

    DEFINITE_IND = feat("Definite", "Ind")
    DEFINITE_SPEC = feat("Definite", "Spec")
    DEFINITE_DEF = feat("Definite", "Def")

    DEGREE_POS = feat("Degree", "Pos")
    DEGREE_CMP = feat("Degree", "Cmp")
    DEGREE_SUP = feat("Degree", "Sup")

    EVIDENT_FH = feat("Evident", "Fh")
    EVIDENT_NFH = feat("Evident", "Nfh")

    FOREIGN_YES = feat("Foreign", "Yes")

    GENDER_MASC = feat("Gender", "Masc")
    GENDER_FEM = feat("Gender", "Fem")

    MOOD_IND = feat("Mood", "Ind")
    MOOD_IMP = feat("Mood", "Imp")
    MOOD_CND = feat("Mood", "Cnd")
    MOOD_QOT = feat("Mood", "Qot")
    MOOD_NEC = feat("Mood", "Nec")

    NUMTYPE_CARD = feat("NumType", "Card")
    NUMTYPE_ORD = feat("NumType", "Ord")
    NUMTYPE_MULT = feat("NumType", "Mult")
    NUMTYPE_FRAC = feat("NumType", "Frac")

    NUMBER_SING = feat("Number", "Sing")
    NUMBER_PLUR = feat("Number", "Plur")
    NUMBER_PTAN = feat("Number", "Ptan")
    NUMBER_COLL = feat("Number", "Coll")

    PERSON_1 = feat("Person", "1")
    PERSON_2 = feat("Person", "2")
    PERSON_3 = feat("Person", "3")

    POLARITY_POS = feat("Polarity", "Pos")
    POLARITY_NEG = feat("Polarity", "Neg")

    POSS_YES = feat("Poss", "Yes")

    PRONTYPE_PRS = feat("PronType", "Prs")
    PRONTYPE_RCP = feat("PronType", "Rcp")
    PRONTYPE_INT = feat("PronType", "Int")
    PRONTYPE_REL = feat("PronType", "Rel")
    PRONTYPE_DEM = feat("PronType", "Dem")
    PRONTYPE_TOT = feat("PronType", "Tot")
    PRONTYPE_NEG = feat("PronType", "Neg")
    PRONTYPE_IND = feat("PronType", "Ind")

    REFLEX_YES = feat("Reflex", "Yes")

    TENSE_PAST = feat("Tense", "Past")
    TENSE_PRES = feat("Tense", "Pres")
    TENSE_FUT = feat("Tense", "Fut")

    VERBFORM_FIN = feat("VerbForm", "Fin")
    VERBFORM_INF = feat("VerbForm", "Inf")
    VERBFORM_PART = feat("VerbForm", "Part")
    VERBFORM_CONV = feat("VerbForm", "Conv")
    VERBFORM_VNOUN = feat("VerbForm", "Vnoun")

    VOICE_ACT = feat("Voice", "Act")
    VOICE_PASS = feat("Voice", "Pass")


def get_upostag(lemma, xpostag):
    """https://github.com/LUMII-AILab/CorporaTools/blob/e84fcb252b5664454c7787a4cff84daa2e14175b/LVTB2UD/src/lv/ailab/lvtb/universalizer/transformator/morpho/UPosLogic.java"""
    lemma = lemma or ''
    xpostag = xpostag or ''

    if re.match("N/[Aa]", xpostag): return UDv2PosTag.X
    if re.match("nc.*", xpostag): return UDv2PosTag.NOUN    # Or sometimes SCONJ
    if re.match("np.*", xpostag): return UDv2PosTag.PROPN;
    if re.match("v[c].*", xpostag) and re.match("(ne)?būt", lemma): return UDv2PosTag.AUX
    if re.match("v[t].*", xpostag) and re.match("(ne)?(kļūt|tikt|tapt)", lemma): return UDv2PosTag.AUX
    if re.match("v.*", xpostag): return UDv2PosTag.VERB
    # elif re.match("v..[^p].*", xpostag): return UDv2PosTag.VERB;
    # elif re.match("v..p[dpu].*", xpostag): return UDv2PosTag.VERB;
    if re.match("a.*", xpostag):
        if lemma in ('manējais', 'tavējais', 'mūsējais', 'jūsējais', 'viņējais', 'savējais', 'daudzi', 'vairāki',
                     'manējā', 'tavējā', 'mūsējā', 'jūsējā', 'viņējā', 'savējā', 'daudzas', 'vairākas'):
            return UDv2PosTag.PRON
        else: return UDv2PosTag.ADJ
    if re.match("p[pxd].*", xpostag): return UDv2PosTag.PRON
    if re.match("p[siqgr].*", xpostag): return UDv2PosTag.PRON
    if re.match("r.*", xpostag): return UDv2PosTag.ADV  # Or sometimes SCONJ
    if re.match("m[cf].*", xpostag): return UDv2PosTag.NUM
    if re.match("mo.*", xpostag): return UDv2PosTag.ADJ
    if re.match("s.*", xpostag): return UDv2PosTag.ADP
    if re.match("cc.*", xpostag): return UDv2PosTag.CCONJ
    if re.match("cs.*", xpostag): return UDv2PosTag.SCONJ
    if re.match("i.*", xpostag): return UDv2PosTag.INTJ
    if re.match("q.*", xpostag): return UDv2PosTag.PART
    if re.match("z.*", xpostag): return UDv2PosTag.PUNCT
    if re.match("yn.*", xpostag): return UDv2PosTag.NOUN
    if re.match("yp.*", xpostag): return UDv2PosTag.PROPN
    if re.match("ya.*", xpostag): return UDv2PosTag.ADJ
    if re.match("yv.*", xpostag): return UDv2PosTag.VERB
    if re.match("yr.*", xpostag): return UDv2PosTag.ADV
    if re.match("yd.*", xpostag): return UDv2PosTag.SYM
    if re.match("xf.*", xpostag): return UDv2PosTag.X   # Or sometimes PROPN / NOUN
    if re.match("xn.*", xpostag): return UDv2PosTag.NUM
    if re.match("xo.*", xpostag): return UDv2PosTag.ADJ
    if re.match("xu.*", xpostag): return UDv2PosTag.SYM
    if re.match("xx.*", xpostag): return UDv2PosTag.SYM # Or sometimes PROPN / NOUN

    logger.warning('Could not obtain UPOSTAG for "%s" with XPOSTAG "%s"', lemma, xpostag)
    return UDv2PosTag.X


def get_synt_upostag(lemma, xpostag, deprel):
    """https://github.com/LUMII-AILab/CorporaTools/blob/e84fcb252b5664454c7787a4cff84daa2e14175b/LVTB2UD/src/lv/ailab/lvtb/universalizer/transformator/morpho/UPosLogic.java"""
    lemma = lemma or ''
    xpostag = xpostag or ''

    if re.match("a.*", xpostag):
        if lemma in ('manējais', 'tavējais', 'mūsējais', 'jūsējais', 'viņējais', 'savējais', 'daudzi', 'vairāki',
                     'manējā', 'tavējā', 'mūsējā', 'jūsējā', 'viņējā', 'savējā', 'daudzas', 'vairākas'):
            return UDv2PosTag.DET if deprel == UDv2Relations.DET else UDv2PosTag.PRON
        else:
            return UDv2PosTag.ADJ

    if re.match("p[dsiqgr].*", xpostag):
        return UDv2PosTag.DET if deprel == UDv2Relations.DET else UDv2PosTag.PRON

    return get_upostag(lemma, xpostag)


def get_ufeats(lemma, xpostag):
    """https://github.com/LUMII-AILab/CorporaTools/blob/b92c740e357426d173905ce29a825dc2deb932c0/LVTB2UD/src/lv/ailab/lvtb/universalizer/transformator/morpho/FeatsLogic.java"""
    lemma = lemma or ''
    xpostag = xpostag or ''
    res = []

    # Inflectional features: nominal
    if re.match("[na].m.*|v..p.m.*|[pm]..m.*", xpostag): res.append(UDv2Feat.GENDER_MASC)
    if re.match("[na].f.*|v..p.f.*|[pm]..f.*", xpostag): res.append(UDv2Feat.GENDER_FEM)
    
    if re.match("[na]..s.*|v..[^p]....s.*|v..p..s.*|[pm]...s.*", xpostag): res.append(UDv2Feat.NUMBER_SING)
    if re.match("[na]..p.*|v..[^p]....p.*|v..p..p.*|[pm]...p.*", xpostag): res.append(UDv2Feat.NUMBER_PLUR)
    if re.match("n..d.*", xpostag): res.append(UDv2Feat.NUMBER_PTAN)   # Fuzzy borders.
    if re.match("n..v.*", xpostag): res.append(UDv2Feat.NUMBER_COLL)   # Fuzzy borders.
    
    if re.match("[na]...n.*|v..p...n.*|[pm]....n.*", xpostag): res.append(UDv2Feat.CASE_NOM)
    if re.match("[na]...a.*|v..p...a.*|[pm]....a.*", xpostag): res.append(UDv2Feat.CASE_ACC)
    if re.match("[na]...d.*|v..p...d.*|[pm]....d.*", xpostag): res.append(UDv2Feat.CASE_DAT)
    if re.match("[na]...g.*|v..p...g.*|[pm]....g.*", xpostag): res.append(UDv2Feat.CASE_GEN)
    if re.match("[na]...l.*|v..p...l.*|[pm]....l.*", xpostag): res.append(UDv2Feat.CASE_LOC)
    if re.match("[na]...v.*|v..p...v.*", xpostag): res.append(UDv2Feat.CASE_VOC)
    
    if re.match("a.....n.*|v..p......n.*", xpostag): res.append(UDv2Feat.DEFINITE_IND)
    if re.match("mo.*", xpostag) and re.match("(treš|ceturt|piekt|sest|septīt|astot|devīt)[sa]", lemma): res.append(UDv2Feat.DEFINITE_SPEC)
    if re.match("a.....y.*|v..p......y.*", xpostag): res.append(UDv2Feat.DEFINITE_DEF)
    if re.match("mo.*", xpostag) and not re.match("(treš|ceturt|piekt|sest|septīt|astot|devīt)[sa]", lemma): res.append(UDv2Feat.DEFINITE_DEF)
    
    # if re.match("a.....p.*|rp.*|v.ypd.*", xpostag): res.append(UDv2Feat.DEGREE_POS)
    if re.match("a.....p.*|v..pd......p.*|rp.*|mo.*", xpostag): res.append(UDv2Feat.DEGREE_POS)
    if re.match("a.....c.*|v..pd......c.*|rc.*", xpostag): res.append(UDv2Feat.DEGREE_CMP)
    if re.match("a.....s.*|v..pd......s.*|rs.*", xpostag): res.append(UDv2Feat.DEGREE_SUP)
    # Patalogical cases like "pirmākais un vispirmākais" are not represented.
    
    # Inflectional features: verbal
    
    # if re.match("v..[^p]....[123].*", xpostag): res.append(UDv2Feat.VERBFORM_FIN) # According to local understanding
    if re.match("v..[^pn].*", xpostag): res.append(UDv2Feat.VERBFORM_FIN)    # According to UD rule of thumb.
    if re.match("v..n.*", xpostag): res.append(UDv2Feat.VERBFORM_INF)
    if re.match("v..pd.*", xpostag): res.append(UDv2Feat.VERBFORM_PART)
    if re.match("a.*", xpostag) and re.match(".*?oš[sa]", lemma): res.append(UDv2Feat.VERBFORM_PART) # Some deverbal adjectives slip unmarked.
    if re.match("v..p[pu].*", xpostag): res.append(UDv2Feat.VERBFORM_CONV)
    if re.match("n.....4.*", xpostag) and lemma.endswith("šana"): res.append(UDv2Feat.VERBFORM_VNOUN)
    if re.match("n.....r.*", xpostag) and lemma.endswith("šanās"): res.append(UDv2Feat.VERBFORM_VNOUN)
    
    if re.match("v..i.*", xpostag): res.append(UDv2Feat.MOOD_IND)
    if re.match("v..m.*", xpostag): res.append(UDv2Feat.MOOD_IMP)
    if re.match("v..c.*", xpostag): res.append(UDv2Feat.MOOD_CND)
    if re.match("v..r.*", xpostag): res.append(UDv2Feat.MOOD_QOT)
    if re.match("v..d.*", xpostag): res.append(UDv2Feat.MOOD_NEC)
    
    if re.match("v..[^p]s.*|v..pd....s.*", xpostag): res.append(UDv2Feat.TENSE_PAST)
    if re.match("v..[^p]p.*|v..pd....p.*", xpostag): res.append(UDv2Feat.TENSE_PRES)
    if re.match("v..[^p]f.*", xpostag): res.append(UDv2Feat.TENSE_FUT)
    
    if re.match("v..pd...ap.*", xpostag): res.append(UDv2Feat.ASPECT_IMP)
    if re.match("v..pd....s.*", xpostag): res.append(UDv2Feat.ASPECT_PERF)
    
    if re.match("v..[^p].....a.*|v..p.....a.*", xpostag): res.append(UDv2Feat.VOICE_ACT)
    if re.match("a.*", xpostag) and re.match(".*?oš[sa]", lemma): res.append(UDv2Feat.VOICE_ACT)    # Some deverbal adjectives slip unmarked.
    if re.match("v..[^p].....p.*|v..p.....p.*", xpostag): res.append(UDv2Feat.VOICE_PASS)   # Some deverbal adjectives slip unmarked.
    
    if re.match("v..i.*", xpostag): res.append(UDv2Feat.EVIDENT_FH)
    if re.match("v..r.*", xpostag): res.append(UDv2Feat.EVIDENT_NFH)
    
    if re.match("p.1.*|v..[^p]...1.*", xpostag): res.append(UDv2Feat.PERSON_1)
    if re.match("a.*", xpostag) and re.match("(man|mūs)ēj(ais|ā)", lemma): res.append(UDv2Feat.PERSON_1)
    if re.match("p.2.*|v..[^p]...2.*", xpostag): res.append(UDv2Feat.PERSON_2)
    if re.match("a.*", xpostag) and re.match("(tav|jūs)ēj(ais|ā)", lemma): res.append(UDv2Feat.PERSON_2)
    if re.match("p.3.*|v..[^p]...3.*", xpostag): res.append(UDv2Feat.PERSON_3)
    if re.match("a.*", xpostag) and re.match("viņēj(ais|ā)", lemma): res.append(UDv2Feat.PERSON_3)
    
    # Minimal annotations, for nomens manual labor is needed.
    if re.match("v..[^p]......n.*", xpostag): res.append(UDv2Feat.POLARITY_POS)
    if re.match("v..p........n.*", xpostag): res.append(UDv2Feat.POLARITY_POS)
    if re.match("q.*", xpostag) and re.match("jā", lemma): res.append(UDv2Feat.POLARITY_POS)
    if re.match("v..[^p]......y.*", xpostag): res.append(UDv2Feat.POLARITY_NEG)
    if re.match("v..p........y.*", xpostag): res.append(UDv2Feat.POLARITY_NEG)
    if re.match("q.*", xpostag) and re.match("n[eē]", lemma): res.append(UDv2Feat.POLARITY_NEG)
    if re.match("cc.*", xpostag) and re.match("ne", lemma): res.append(UDv2Feat.POLARITY_NEG)
    # if re.match("is.*", xpostag) and re.match("n[eē]", lemma): res.append(UDv2Feat.POLARITY_NEG)
    
    # Lexical features
    if re.match("p[ps].*", xpostag): res.append(UDv2Feat.PRONTYPE_PRS)
    if re.match("a.*", xpostag) and re.match("(man|mūs|tav|jūs|viņ|sav)ēj(ais|ā)", lemma): res.append(UDv2Feat.PRONTYPE_PRS)
    if re.match("px.*", xpostag): res.append(UDv2Feat.PRONTYPE_RCP)
    if re.match("pq.*", xpostag): res.append(UDv2Feat.PRONTYPE_INT)
    if re.match("r0.*", xpostag) and re.match("(ne)?(cik|kad|kā|kurp?|kāpēc|kādēļ|kālab(ad)?)", lemma): res.append(UDv2Feat.PRONTYPE_INT)
    
    if re.match("pr.*", xpostag): res.append(UDv2Feat.PRONTYPE_REL)
    if re.match("pd.*", xpostag): res.append(UDv2Feat.PRONTYPE_DEM)
    if re.match("r0.*", xpostag) and re.match("(ne)?(te|tur|šeit|tad|tagad|tik|tā)", lemma): res.append(UDv2Feat.PRONTYPE_DEM)
    
    if re.match("pg.*", xpostag): res.append(UDv2Feat.PRONTYPE_TOT)
    if re.match("r0.*", xpostag) and re.match("vienmēr|visur|visad(iņ)?", lemma): res.append(UDv2Feat.PRONTYPE_TOT)
    
    if re.match("p.....y.*", xpostag): res.append(UDv2Feat.PRONTYPE_NEG)
    if re.match("r0.*", xpostag) and re.match("ne.*", lemma): res.append(UDv2Feat.PRONTYPE_NEG)
    
    if re.match("pi.*", xpostag): res.append(UDv2Feat.PRONTYPE_IND)
    
    if re.match("mc.*|xn.*", xpostag): res.append(UDv2Feat.NUMTYPE_CARD)    # Nouns like "simts", "desmits" are not marked.
    if re.match("mo.*|xo.*", xpostag): res.append(UDv2Feat.NUMTYPE_ORD)
    if re.match("r0.*", xpostag) and re.match("(vien|div|trīs|četr|piec|seš|septiņ|astoņ|deviņ|desmit|pusotr)reiz", lemma): res.append(UDv2Feat.NUMTYPE_MULT)   # Incomplete list.
    if re.match("mf.*", xpostag): res.append(UDv2Feat.NUMTYPE_FRAC) # Nouns like "desmitdaļa" are not marked.
    
    if re.match("ps.*", xpostag): res.append(UDv2Feat.POSS_YES)
    if re.match("a.*", xpostag) and re.match("(man|mūs|tav|jūs|viņ|sav)ēj(ais|ā)", lemma): res.append(UDv2Feat.POSS_YES)
    
    if re.match("xf.*", xpostag): res.append(UDv2Feat.FOREIGN_YES)
    if re.match("y.*", xpostag): res.append(UDv2Feat.ABBR_YES)
    if re.match("px.*|v.y.*", xpostag): res.append(UDv2Feat.REFLEX_YES) # Currently it is impossible to split out "reflexive particle" of each verb.

    return res


def get_synt_ufeats(lemma, xpostag, has_sel_disc_child=False, has_case_child=True):
    """https://github.com/LUMII-AILab/CorporaTools/blob/b92c740e357426d173905ce29a825dc2deb932c0/LVTB2UD/src/lv/ailab/lvtb/universalizer/transformator/morpho/FeatsLogic.java"""
    lemma = lemma or ''
    xpostag = xpostag or ''
    res = get_ufeats(lemma, xpostag)

    if not (re.match("n.*", xpostag) and re.match("kuriene|t(ur|ej)iene|vis(ur|ad)iene|nek(ur|ad)iene", lemma)) \
            and not re.match("r0.*", xpostag):
        return res

    # TODO

    if re.match("r0.*", xpostag) and has_sel_disc_child:
        res.append(UDv2Feat.PRONTYPE_IND)
        try:
            res.remove(UDv2Feat.PRONTYPE_INT)
        except: pass
    elif re.match("n.*", xpostag) and has_case_child:
        if lemma == 'kuriene': res.append(UDv2Feat.PRONTYPE_IND if has_case_child else UDv2Feat.PRONTYPE_INT)
        if lemma == 'turiene' or lemma == 'tejiene': res.append(UDv2Feat.PRONTYPE_DEM)
        if lemma == 'visuriene' or lemma == 'visadiene': res.append(UDv2Feat.PRONTYPE_TOT)
        if lemma == 'nekuriene' or lemma == 'nekadiene': res.append(UDv2Feat.PRONTYPE_NEG)

    return res


def feats_str(feats):
    feats.sort(key=lambda e: (e.name.lower(), e.value))
    parts = []
    prev_feat_name = None
    for f in feats:
        if f.name == prev_feat_name:
            parts.append(',')
            parts.append(f.value)
        else:
            parts.append('|')
            parts.append(f.name)
            parts.append('=')
            parts.append(f.value)
        prev_feat_name = f.name
    return ''.join(parts[1:]) or '_'


def check(ud_file_url):
    print('Check ', ud_file_url)
    import urllib.request
    response = urllib.request.urlopen(ud_file_url)
    lines = response.read().decode().splitlines()

    error_count = 0
    for i, line in enumerate(lines, start=1):
        if not line: continue
        if line.startswith('#'): continue
        tokens = line.split('\t')
        lemma = tokens[2]
        upostag = tokens[3]
        xpostag = tokens[4]
        deprel = tokens[7]
        feats = tokens[5]

        upostag_calc = get_synt_upostag(lemma, xpostag, deprel)
        if upostag_calc != upostag:
            print('WARN UPOSTAG: #{i} "{lemma}" "{xpostag}" "{deprel}": "{upostag}" != "{upostag_calc}"'.format(i=i, lemma=lemma, xpostag=xpostag, deprel=deprel, upostag=upostag, upostag_calc=upostag_calc))
            error_count += 1

        # feats_calc = feats_str(get_synt_ufeats(lemma, xpostag))
        feats_calc = feats_str(get_ufeats(lemma, xpostag))
        if feats_calc != feats:
            print('WARN FEATS: #{i} "{lemma}" "{xpostag}": "{feats}" != "{feats_calc}"'.format(i=i, lemma=lemma, xpostag=xpostag, feats=feats, feats_calc=feats_calc))
            error_count += 1

        if error_count > 200:
            print('... Stop due too many errors line=%d' % i)
            break

    print('Error count:', error_count)


if __name__ == '__main__':
    # print(feats_str([UDv2Feat.PRONTYPE_NEG, UDv2Feat.PRONTYPE_IND, UDv2Feat.CASE_ACC]))
    check('https://raw.githubusercontent.com/UniversalDependencies/UD_Latvian-LVTB/7028b14ee6f328e226577637f4e3f0343c963ba9/lv_lvtb-ud-dev.conllu')
    check('https://raw.githubusercontent.com/UniversalDependencies/UD_Latvian-LVTB/7028b14ee6f328e226577637f4e3f0343c963ba9/lv_lvtb-ud-test.conllu')
    check('https://raw.githubusercontent.com/UniversalDependencies/UD_Latvian-LVTB/7028b14ee6f328e226577637f4e3f0343c963ba9/lv_lvtb-ud-train.conllu')
