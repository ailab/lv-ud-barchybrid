#!/usr/bin/env bash

IMG=registry.gitlab.com/ailab/lv-ud-barchybrid

if [ $# -eq 0 ]; then
    TAG=latest
    echo "Build local ${IMG}:${TAG}"
    docker build -t $IMG:$TAG .
fi

if [ "$1" == "push" ]; then
    TAG=${2:-latest}
    echo "Push ${IMG}:${TAG}"
    docker tag $IMG:latest $IMG:$TAG \
    && docker push $IMG:$TAG
fi

if [ "$1" == "build-base" ] ; then
    docker build -t artursz/dynet -f Dockerfile.base .
fi

if [ "$1" == "push-base" ] ; then
    TAG=${TAG:-v1}
    docker tag artursz/dynet artursz/dynet:$TAG \
        && docker push artursz/dynet:$TAG
fi
