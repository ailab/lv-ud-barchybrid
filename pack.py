import argparse
import sys
from pathlib import Path
import zipfile


def get_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('model_dir', help='Model directory')
    return vars(parser.parse_args(args))


def pack(model_dir, out_zip=None):
    d = Path(model_dir)

    best_epoch = None
    best_las = 0
    with (d / 'res.csv').open() as f:
        next(f)
        for l in f:
            epoch, las, uas, wlas, tlas, tuas, twlas = [float(x) for x in l.strip().split()]
            epoch = int(epoch)
            if best_las <= las:
                best_las = las
                best_epoch = epoch + 1
    print('Best epoch', best_epoch, 'LAS=', best_las)

    out_zip = out_zip or model_dir + ".zip"
    zf = zipfile.ZipFile(out_zip, "w", zipfile.ZIP_DEFLATED)
    for f in d.iterdir():
        if f.name == 'barchybrid.model{}'.format(best_epoch):
            print(f.name)
            zf.write(str(f), 'barchybrid.model')
        if f.name in ['params.pickle', 'res.csv', 'log.txt'] or '_{}.conllu'.format(best_epoch) in f.name:
            print(f.name)
            zf.write(str(f), f.name)
    zf.close()


if __name__ == '__main__':
    print('pack', sys.argv[1])
    pack(sys.argv[1])
